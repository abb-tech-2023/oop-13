package src;

import src.Task1.Car;
import src.Task4.CarAbstract;
import src.Task4.EconomyCar;
import src.Task4.LuxuryCar;

public class Main {
    public static void main(String[] args) {


        //Task1 :
       /* Car car1 = new Car();

        // Set the car details
        car1.setMake("Audi");
        car1.setModel("RS7");
        car1.setYear(2021);
        car1.setRentalPrice(135.0);

        // Get and display the car details
        System.out.println("Car Details:");
        System.out.println("Make: " + car1.getMake());
        System.out.println("Model: " + car1.getModel());
        System.out.println("Year: " + car1.getYear());
        System.out.println("Rental Price: $" + car1.getRentalPrice());*/

        //Task2 :
 /* Manager manager = new Manager("Nurlana",5000,"ABB");
        manager.getdetails ();
       Developer developer = new Developer("nurlana",6000,"java");
        developer.getdetails();

  */

        //Task3 :
        /*
        Playable audioPlayer = new AudioPlayer();
        audioPlayer.play();
        audioPlayer.stop();

        Playable videoPlayer = new VideoPlayer();
        videoPlayer.play();
        videoPlayer.stop();*/

        //Task4
        EconomyCar economyCar = new EconomyCar("Tayota", "Corolla", 2021, 50, 10);
        economyCar.displayCarInfo();
        double economyCarRentalCharge = economyCar.calculateRentalCharge(7);
        System.out.println("Rental Charge: $" + economyCarRentalCharge);
        LuxuryCar luxuryCar = new LuxuryCar("Mercedes-Benz", "S-Class", 2022, 50, "Leather Seats");
        luxuryCar.displayCarInfo();
        double luxuryCarRentalCharge = luxuryCar.calculateRentalCharge(3);
        System.out.println("Rental Charge: $" + luxuryCarRentalCharge);

        CarAbstract car = new CarAbstract("AUDI", "RS7", 2021, 135.0) {
            @Override
            public boolean rent(int numDays) {
                return false;
            }

            @Override
            public boolean returnCar() {
                return false;
            }

            @Override
            public double calculateRentalCharge(int numDays) {
                return 0;
            }
        };

        car.setMake("Audi");
        car.setModel("RS7");
        car.setYear(2021);
        car.setRentalRate(0.8);


        System.out.println("Car Details:");
        System.out.println("Make: " + car.getMake());
        System.out.println("Model: " + car.getModel());
        System.out.println("Year: " + car.getYear());
        System.out.println("Rental Rate: $" + car.getRentalRate());
    }

}

