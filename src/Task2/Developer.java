package src.Task2;

public class Developer extends Employee {
    private String programmingLanguage;

    public Developer(String name, double salary, String programmingLanguage) {
        super(name, salary);
        this.programmingLanguage = programmingLanguage;
    }

    @Override
    public void getdetails() {
        System.out.println("name,salary,programmingLanguage");
    }
}

