package src.Task3;

public interface Playable {
    void  play();
    void stop();
}
