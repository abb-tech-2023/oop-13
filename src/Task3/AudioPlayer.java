package src.Task3;

public class AudioPlayer implements Playable  {
    @Override
    public void play() {
        System.out.println("Audio player is playing.");
    }

    @Override
    public void stop() {
        System.out.println("Audio player stopped.");
    }
}

