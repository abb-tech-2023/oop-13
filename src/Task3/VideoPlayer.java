package src.Task3;

public class VideoPlayer implements Playable{
    @Override
    public void play() {
        System.out.println("Video player is playing.");
    }

    @Override
    public void stop() {
        System.out.println("Video player stopped.");
    }
}
